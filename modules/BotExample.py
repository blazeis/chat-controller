import logging

from libs.utils import reset_user_data, get_access_level, get_keyboard, parse_message
from modules.BotBase import BotBase


class BotInfo(BotBase):
    def __init__(self, bot):
        super().__init__(keyword='example', bot=bot)

    def handle_callback_info(self, update, context, level, user, query):
        reset_user_data(context.user_data)
        text = "Привет. Я бот для работы с маркетплейсаим. " \
               "Вот видео, где я расскажу что умею: https://www.youtube.com/watch?v=3bADd57CS48\n" \
               "А нажав на кнопки ниже, ты подробно узнаешь о моих услугах"
        keyboard = get_keyboard(level, 'base')
        query.edit_message_text(text=text, reply_markup=keyboard)

    def handle_userdata_operator(self, update, context, level, user):
        message_text, message_type, file_id = parse_message(update.message)
        keyboard = get_keyboard(get_access_level(user.id), 'base')
        self.sendAnyMessage(user.id, message_text, message_type, file_id, reply_markup=keyboard)
