import logging
from time import time

from peewee import fn
from telegram.utils.helpers import escape_markdown

from libs.Managers.DBManager import manage_connection, User, Messages, Carma
from libs.static import AccessLevel, kb_user, MessageType, carma_words


def reset_user_data(user_data):
    """
    сбрасывает все сохраненные контекстные переменные
        :param user_data: словарь с контекстом, обычно context.user_data
    """
    user_data.pop('action', None)


@manage_connection
def get_access_level(user_id):
    """
    Возвращает уровень доступа сотрудника
    :param user_id: telegram ID пользователя
    :return: AccessLevel
    """
    res = User.select(User.access_level).where(User.tg_id == user_id).limit(1)
    level = res[0].access_level if len(res) > 0 else AccessLevel.UNKNOWN.value
    if level is None:
        level = AccessLevel.UNKNOWN.value
    return AccessLevel(level)


def get_keyboard(role, point):
    """
    Возвращает Inline-разметку клавиатуры для нужной роли и запрошенной точки
    :param role: уровень доступа пользователя, AccessLevel
    :param point: запрашиваемая точка, например 'base'
    :return: Клавиатура
    """
    if role == AccessLevel.USER:
        return kb_user[point]
    elif role == AccessLevel.OPERATOR:
        return kb_user[point]
    elif role == AccessLevel.ADMIN:
        return kb_user[point]
    else:
        return None


def parse_message(message):
    """
    Парсит сообщение на предмет типа
    :param message: message
    :return: message_text, message_type, file_id
    """
    message_text = message.text_markdown_v2 if message.text_markdown_v2 else \
        message.caption_markdown_v2 if message.caption_markdown_v2 else ""
    if message.text:
        message_type = MessageType.TEXT
        file_id = None
    elif message.audio:
        message_type = MessageType.AUDIO
        file_id = message.audio.file_id
    elif message.video:
        message_type = MessageType.VIDEO
        file_id = message.video.file_id
    elif message.photo:
        message_type = MessageType.PHOTO
        if type(message.photo) == list:
            add_photo = None
            for photo in message.photo:
                if add_photo is None:
                    add_photo = photo
                else:
                    if photo.file_size > add_photo.file_size:
                        add_photo = photo
            file_id = add_photo.file_id
        else:
            file_id = message.photo.file_id
    elif message.animation:
        message_type = MessageType.ANIMATION
        file_id = message.animation.file_id
    elif message.document:
        message_type = MessageType.DOCUMENT
        file_id = message.document.file_id
    elif message.sticker:
        message_type = MessageType.STICKER
        file_id = message.sticker.file_id
    else:
        logging.info(f'Unknown message type: {message}')
        message_type = MessageType.UNKNOWN
        file_id = None
    return message_text, message_type, file_id


def getAnyUserName(user):
    return user.first_name if user.first_name else user.username if user.username else user.id


@manage_connection
def save_callback(callback_data, user_id):
    """
    Сохраняет callback_data в базу для истории
    :param callback_data: собственно callback_data
    :param user_id: telegram ID пользователя, нажавшего кнопку
    """
    Messages.insert(date=time(),
                    sender_id=user_id,
                    text=callback_data,
                    reply_to=None,
                    file_id=None,
                    forward_from=None,
                    message_type=MessageType.CALLBACK.value
                    ).execute()


@manage_connection
def update_access_level(tg_id, tg_name, tg_user, level):
    """
    Устанавливает уровень доступа и заносит данные пользователя в табличку
    :param tg_id: telegram ID пользователя
    :param tg_name: telegram Name пользователя
    :param tg_user: telegram @username пользователя
    :param level: новый уровень доступа
    """
    User.insert(tg_id=tg_id,
                tg_name=tg_name,
                tg_user=tg_user,
                access_level=level.value
                ).on_conflict(preserve=[User.tg_name,
                                        User.tg_user,
                                        User.access_level]
                              ).execute()


@manage_connection
def save_message(message):
    """
    Сохраняет сообщение в базе для истории
    :param message: message (из update.message)
    """
    message_text, message_type, file_id = parse_message(message)
    message_dict = message.to_dict()
    Messages.insert(date=message_dict.get('date', int(time())),
                    group_id=message.chat.id,
                    sender_id=message_dict.get("from", {}).get("id", -1),
                    text=message_text,
                    reply_to=message_dict.get("reply_to_message", {}).get("message_id", None),
                    file_id=file_id,
                    forward_from=message_dict.get("forward_from_chat", {}).get("id", None),
                    message_type=message_type.value
                    ).execute()


@manage_connection
def add_carma(sender, receiver, chat, message_id):
    dup = Carma.get_or_none(Carma.tg_sender_id == sender,
                            Carma.tg_chat == chat,
                            Carma.tg_receiver_id == receiver,
                            Carma.message_id == message_id)
    if dup is None:
        Carma.insert(tg_sender_id=sender,
                     tg_receiver_id=receiver,
                     tg_chat=chat,
                     message_id=message_id,
                     date=time()).execute()
        return True
    else:
        return False


@manage_connection
def get_carma(chat_id, user_id):
    res = Carma.select(fn.COUNT(Carma.tg_receiver_id).alias('count')).where(Carma.tg_receiver_id == user_id, Carma.tg_chat == chat_id)
    if res is not None:
        return res[0].count


@manage_connection
def calculate_carma_top(chat):
    res = Carma.select(Carma.tg_receiver_id.alias('tg_id'),
                       fn.COUNT(Carma.tg_receiver_id).alias('count'))\
        .where(Carma.tg_chat == chat).group_by(Carma.tg_receiver_id)
    result = {}
    for elem in res:
        result[elem.tg_id] = elem.count
    return {k: v for k, v in sorted(result.items(), key=lambda item: item[1], reverse=True)}


def is_carma_contained(text: str):
    text_prepared = text.lower().strip()
    for cw in carma_words:
        if cw in text_prepared:
            return True
    return False


@manage_connection
def get_tg_name_by_id(tg_id):
    ret = tg_id
    res = User.get_or_none(User.tg_id == tg_id, User.access_level > AccessLevel.UNKNOWN.value)
    if res is not None:
        tg_user = res.tg_user
        if tg_user is not None:
            ret = tg_user
        else:
            tg_name = res.tg_name
            if tg_name is not None:
                ret = tg_name
    return ret


def escape(text: str):
    if text is not None and isinstance(text, str):
        return escape_markdown(text, version=2)
    else:
        return text
