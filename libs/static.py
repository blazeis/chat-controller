from enum import IntEnum
from telegram import InlineKeyboardMarkup, InlineKeyboardButton


class AccessLevel(IntEnum):
    BANNED = -1
    UNKNOWN = 0  # мы его не знаем
    USER = 1  # все зареганные
    OPERATOR = 2  # саппорт
    ADMIN = 3  # админ


class MessageType(IntEnum):
    UNKNOWN = 0
    TEXT = 1
    AUDIO = 2
    PHOTO = 3
    VIDEO = 4
    DOCUMENT = 5
    ANIMATION = 6
    STICKER = 7
    CALLBACK = 8


carma_words = ["+1",
               "красота",
               "огонь",
               "молодец",
               "спасибо"]

kb_user = {'base': InlineKeyboardMarkup([[InlineKeyboardButton("О боте", callback_data=f'info')]])}

